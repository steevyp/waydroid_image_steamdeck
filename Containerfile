FROM ghcr.io/steamdeckhomebrew/holo-base:latest AS build

ARG KERNEL_PKGS "linux-neptune-61 linux-neptune-61-headers"

RUN pacman -Syu --needed --noconfirm base-devel git $KERNEL_PKGS && \
    yes | pacman -Scc

RUN useradd --system --create-home makepkg && \
    echo "makepkg ALL=(ALL:ALL) NOPASSWD:ALL" > /etc/sudoers.d/makepkg

USER makepkg
WORKDIR /home/makepkg

RUN git clone https://aur.archlinux.org/binder_linux-dkms.git && \
    cd binder_linux-dkms && \
    makepkg -si --noconfirm && \
    cd && \
    rm -rf binder_linux-dkms .cache && \
    yes | sudo pacman -Scc

FROM ghcr.io/vanilla-os/waydroid:main

ARG DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y wlr-randr && \
    apt-get clean

RUN rm -rf /lib/modules
COPY --from=build /lib/modules /lib/modules
ADD --chmod=0755 ewaydroid /usr/bin/ewaydroid

CMD ["/bin/bash"]
