#!/bin/sh

# Remove the performance overlay, it meddles with some tasks
unset LD_PRELOAD

# Add cage to our PATH
CAGE="$HOME/.local/cage"
export LD_LIBRARY_PATH="$CAGE/lib"
export PATH="$CAGE/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"

cage -- /usr/bin/konsole -e ewaydroid show-full-ui
