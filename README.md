# Waydroid on Steam Deck inside of a distrobox
Custom Containerfile to run Waydroid inside of a distrobox on SteamOS. Not for the faint of heart! Most people should probably use ryanrudolfoba's [Waydroid installer](https://github.com/ryanrudolfoba/SteamOS-Waydroid-Installer) instead, but if you want to try Waydroid without modifying the system too much this may be useful.

## Prerequisites

* sudo password set
* Latest version of distrobox (the preinstalled version doesn't support --unshare-all)

## Install cage to the home folder

```bash
mkdir -p ~/.local/cage
curl -sL https://archlinux.org/packages/extra/x86_64/cage/download/ | tar xv --zstd --strip-components=1 -C ~/.local/cage
curl -sL https://archlinux.org/packages/extra/x86_64/wlroots/download/ | tar xv --zstd --strip-components=1 -C ~/.local/cage
```

## Build the image
```bash
# Offload /var/lib/containers to home so our var partition doesn't fill up
sudo rm -rf /var/lib/containers
sudo mkdir -p /home/var-lib-containers
sudo ln -sf /home/var-lib-containers /var/lib/containers

sudo podman build -t waydroid-image:latest .
```

## Create the distrobox
```bash
mkdir -p ~/distrobox/waydroid

distrobox create \
    --root \
    --image waydroid-image:latest \
    --init \
    --unshare-all \
    --name waydroid \
    --home ~/distrobox/waydroid \
    --absolutely-disable-root-password-i-am-really-positively-sure
```

## Setting up Waydroid
```bash
# Enter the container
distrobox enter --root waydroid

# Initialize Waydroid
sudo ewaydroid init -s GAPPS
ewaydroid session start &

# Waydroid Extras script
sudo apt install -y lzip python3.11-venv
git clone https://github.com/casualsnek/waydroid_script
cd waydroid_script
python3 -m venv venv
venv/bin/pip install -r requirements.txt
sudo venv/bin/python3 main.py # install libndk and register device with Google Play

# export the waydroid wrapper to the host
distrobox-export -b /usr/bin/ewaydroid
# Leave the container
exit
```

## Adding Waydroid to game mode
```bash
steamos-add-to-steam NestedAndroid.sh
```

## Useful links
* https://github.com/ryanrudolfoba/SteamOS-Waydroid-Installer
* https://github.com/Vanilla-OS/waydroid-image
* https://gist.github.com/Saren-Arterius/c5bc39199552a5c244449b0ce467d6b6
